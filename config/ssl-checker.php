<?php

return [
    'period_alert' => env('SSL_PERIOD_ALERT', '32,16,8,4,3,2,1'),
    'email' => env('SSL_EMAIL', 'info@myopensoft.net'),
];
