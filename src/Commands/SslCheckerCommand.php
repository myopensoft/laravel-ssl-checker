<?php

namespace Myopensoft\SslChecker\Commands;

use Illuminate\Console\Command;
use Myopensoft\SslChecker\SslChecker;

class SslCheckerCommand extends Command
{
    public $signature = 'ssl:check';

    public $description = 'Check SSL health.';

    public function handle()
    {
        SslChecker::bundle();
    }
}
