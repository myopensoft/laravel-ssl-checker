<?php

namespace Myopensoft\SslChecker\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SslCheckerModel extends Model
{
    use SoftDeletes;

    public $table = 'ssl_checkers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = [
        'domain_name',
        'expired_date',
    ];

    protected $casts = [
        'domain_name' => 'string',
        'expired_date' => 'date',
    ];
}

