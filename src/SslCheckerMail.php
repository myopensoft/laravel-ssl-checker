<?php

namespace Myopensoft\SslChecker;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SslCheckerMail extends Mailable
{
    use Queueable, SerializesModels;

    protected string $domainName;
    protected int $daysRemaining;
    protected string $expiredDate;

    public function __construct(string $domainName, int $daysRemaining, string $expiredDate)
    {
        $this->domainName = $domainName;
        $this->daysRemaining = $daysRemaining;
        $this->expiredDate = $expiredDate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('ssl-checker::ssl_checker_reminder')
            ->subject('SSL ' . $this->domainName . ' hampir tamat.')
            ->with([
                'domainName' => $this->domainName,
                'daysRemaining' => $this->daysRemaining,
                'expiredDate' => $this->expiredDate,
            ]);
    }
}
