<?php

namespace Myopensoft\SslChecker;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Myopensoft\SslChecker\Models\SslCheckerModel;

class SslChecker
{
    public static function bundle()
    {
        SslCheckerModel::query()
            ->chunk(10, function ($sites) {
                foreach ($sites as $site) {
                    $today = Carbon::parse();
                    $periodAlerts = explode(',', config('ssl-checker.period_alert'));
                    if (
                        $site->expired_date == null
                        || $site->expired_date->diffInDays($today) <= ((int)max($periodAlerts) + 3)
                    ) {
                        $expiredDate = self::singleDomainChecker($site->domain_name);

                        if ($expiredDate === 'No-SSL') {
                            echo $site->domain_name . ' ' . $expiredDate;
                            continue;
                        }

                        $expiredDateCarbon = Carbon::parse($expiredDate);
                        $daysRemaining = $today->diffInDays($expiredDateCarbon);

                        $site->expired_date = $expiredDateCarbon->toDateString();
                        $site->save();

                        if (in_array($daysRemaining, $periodAlerts)) {
                            Mail::to(config('ssl-checker.email'))
                                ->send(new SslCheckerMail(
                                    $site->domain_name,
                                    $daysRemaining,
                                    $expiredDateCarbon->format('d-m-Y')
                                ));
                        }
                    }
                }
            });
    }

    public static function singleDomainChecker($domainName)
    {
        $url = $domainName;
        $originalParse = parse_url($url, PHP_URL_HOST);
        $get = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));

        // check if domain have SSL
        try {
            $readSsl = fopen("$domainName", "rb", false, $get);
        } catch (\Exception $e) {
            return 'No-SSL';
        }
        
        $contSsl = stream_context_get_params($readSsl);

        if (!isset($contSsl["options"]["ssl"]["peer_certificate"])) {
            return 'No-SSL';
        }

        $read = stream_socket_client("ssl://" . $originalParse . ":443", $errorNumber, $errorMessage, 30, STREAM_CLIENT_CONNECT, $get);

        $cert = stream_context_get_params($read);
        $certInfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate'])['validTo_time_t'];
        return gmdate("Y-m-d", $certInfo);
    }
}
