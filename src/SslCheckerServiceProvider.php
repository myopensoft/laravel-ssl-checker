<?php

namespace Myopensoft\SslChecker;

use Myopensoft\SslChecker\Commands\SslCheckerCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class SslCheckerServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('laravel-ssl-checker')
            ->hasMigration('create_ssl_checkers_table')
            ->hasConfigFile('ssl-checker')
            ->hasCommand(SslCheckerCommand::class)
            ->hasViews();
    }
}
