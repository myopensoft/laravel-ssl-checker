@component('mail::message')
SSL untuk {{$domainName}} akan tamat dalam masa {{$daysRemaining}} hari.

Sila pastikan SSL untuk domain ini dikemaskini sebelum atau pada {{$expiredDate}} untuk mengelakkan sebarang masalah.

Jika mempunyai SSL yang digunakan pakai oleh third-party seperti FPX sila rancang lebih awal.

---

Dihantar oleh [Sago](http://159.223.43.172), Sistem Pengurusan Projek

@endcomponent
