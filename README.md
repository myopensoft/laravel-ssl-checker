# Laravel Health Checker

[![Latest Version on Packagist](https://img.shields.io/packagist/v/myopensoft/laravel-ssl-checker.svg?style=flat-square)](https://packagist.org/packages/myopensoft/laravel-ssl-checker)
[![Total Downloads](https://img.shields.io/packagist/dt/myopensoft/laravel-ssl-checker.svg?style=flat-square)](https://packagist.org/packages/myopensoft/laravel-ssl-checker)

Laravel Health Checker library is for server to send server health info to receiver server and be processed.

## Installation

You can install the package via composer:

```bash
composer require myopensoft/laravel-ssl-checker
```

You can publish and run the migrations with:

You can publish the config file with:
```bash
php artisan vendor:publish --provider="Myopensoft\SslChecker\SslCheckerServiceProvider" --tag="ssl-checker-config"
```

```bash
php artisan vendor:publish
```

select ``Tag: ssl-checker-migrations``

This is the contents of the published config file:

```php
return [
    'period_alert' => env('SSL_PERIOD_ALERT', '32,16,8,4,3,2,1'),
    'email' => env('SSL_EMAIL', 'info@myopensoft.net'),
];
```

## Usage

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](.github/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Muhammad Syafiq Bin Zainuddin](https://github.com/lomotech)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
